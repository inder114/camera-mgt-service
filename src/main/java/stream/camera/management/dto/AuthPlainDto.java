package stream.camera.management.dto;

public class AuthPlainDto {
    private String uuid;
    private String[] ipAddressList;
    private String username;
    private String password;

    public AuthPlainDto() {
        super();
    }

    public AuthPlainDto(String uuid, String[] ipAddressList, String username, String password) {
        this.uuid = uuid;
        this.ipAddressList = ipAddressList;
        this.username = username;
        this.password = password;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String[] getIpAddressList() {
        return ipAddressList;
    }

    public void setIpAddressList(String[] ipAddressList) {
        this.ipAddressList = ipAddressList;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" + " \"uuid\":\"" + uuid + "\"" + ", \"ipAddressList\":[" + buildIpAddressListJson() + "]"
                + ", \"username\":\"" + username + "\"" + ", \"password\":\"" + password + "\"" + "}";
    }

    private String buildIpAddressListJson() {
        String jsonString = "";
        if (ipAddressList != null) {
            for (int i = 0; i < ipAddressList.length; i++) {
                jsonString = jsonString + "\"" + ipAddressList[i] + "\"";
                if (i != ipAddressList.length - 1)
                    jsonString += ",";
            }
        } else
            jsonString = "null";
        return jsonString;
    }
}
